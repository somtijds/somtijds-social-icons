<?php
/**
 * Somtijds Social Icons
 *
 * @link    https://bitbucket.org/somtijds/somtijds-social-icons
 * @package somtijds/somtijds-social-icons
 * @version 0.5.0
 * @TODO Translations, Class-based
 *
 * Plugin Name: Somtijds Social Media Icons
 * Description: This plugin helps to add some social media icons to your theme
 * Author:      Willem Prins
 * Version:     1.0
 * Author URI:  http://www.somtijds.nl
 **/


require_once dirname( __FILE__ ) . '/assets/lib/class-tgm-plugin-activation.php';

add_action( 'tgmpa_register', 'ssi_register_required_plugins' );

/**
 * Register the required plugins for this plugin
 */

function ssi_register_required_plugins() {
	/*
	 * Array of plugin arrays. Required keys are name and slug.
	 * If the source is NOT from the .org repo, then source is also required.
	 */
	$plugins = array(

		// This is an example of how to include a plugin from the WordPress Plugin Repository.
		array(
			'name'      => 'Kirki',
			'slug'      => 'kirki',
			'required'  => false,
		),

	);

	$config = array(
		'id'           => 'somtijds-social-icons',                 // Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',                      // Default absolute path to bundled plugins.
		'menu'         => 'tgmpa-install-plugins', // Menu slug.
		'parent_slug'  => 'plugins.php',            // Parent menu slug.
		'capability'   => 'manage_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
		'has_notices'  => true,                    // Show admin notices or not.
		'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => false,                   // Automatically activate plugins after installation or not.
		'message'      => '',                      // Message to output right before the plugins table.

		/*
		'strings'      => array(
			'page_title'                      => __( 'Install Required Plugins', 'somtijds-social-icons' ),
			'menu_title'                      => __( 'Install Plugins', 'somtijds-social-icons' ),
			/* translators: %s: plugin name. * /
			'installing'                      => __( 'Installing Plugin: %s', 'somtijds-social-icons' ),
			/* translators: %s: plugin name. * /
			'updating'                        => __( 'Updating Plugin: %s', 'somtijds-social-icons' ),
			'oops'                            => __( 'Something went wrong with the plugin API.', 'somtijds-social-icons' ),
			'notice_can_install_required'     => _n_noop(
				/* translators: 1: plugin name(s). * /
				'This theme requires the following plugin: %1$s.',
				'This theme requires the following plugins: %1$s.',
				'somtijds-social-icons'
			),
			'notice_can_install_recommended'  => _n_noop(
				/* translators: 1: plugin name(s). * /
				'This theme recommends the following plugin: %1$s.',
				'This theme recommends the following plugins: %1$s.',
				'somtijds-social-icons'
			),
			'notice_ask_to_update'            => _n_noop(
				/* translators: 1: plugin name(s). * /
				'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.',
				'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.',
				'somtijds-social-icons'
			),
			'notice_ask_to_update_maybe'      => _n_noop(
				/* translators: 1: plugin name(s). * /
				'There is an update available for: %1$s.',
				'There are updates available for the following plugins: %1$s.',
				'somtijds-social-icons'
			),
			'notice_can_activate_required'    => _n_noop(
				/* translators: 1: plugin name(s). * /
				'The following required plugin is currently inactive: %1$s.',
				'The following required plugins are currently inactive: %1$s.',
				'somtijds-social-icons'
			),
			'notice_can_activate_recommended' => _n_noop(
				/* translators: 1: plugin name(s). * /
				'The following recommended plugin is currently inactive: %1$s.',
				'The following recommended plugins are currently inactive: %1$s.',
				'somtijds-social-icons'
			),
			'install_link'                    => _n_noop(
				'Begin installing plugin',
				'Begin installing plugins',
				'somtijds-social-icons'
			),
			'update_link' 					  => _n_noop(
				'Begin updating plugin',
				'Begin updating plugins',
				'somtijds-social-icons'
			),
			'activate_link'                   => _n_noop(
				'Begin activating plugin',
				'Begin activating plugins',
				'somtijds-social-icons'
			),
			'return'                          => __( 'Return to Required Plugins Installer', 'somtijds-social-icons' ),
			'plugin_activated'                => __( 'Plugin activated successfully.', 'somtijds-social-icons' ),
			'activated_successfully'          => __( 'The following plugin was activated successfully:', 'somtijds-social-icons' ),
			/* translators: 1: plugin name. * /
			'plugin_already_active'           => __( 'No action taken. Plugin %1$s was already active.', 'somtijds-social-icons' ),
			/* translators: 1: plugin name. * /
			'plugin_needs_higher_version'     => __( 'Plugin not activated. A higher version of %s is needed for this theme. Please update the plugin.', 'somtijds-social-icons' ),
			/* translators: 1: dashboard link. * /
			'complete'                        => __( 'All plugins installed and activated successfully. %1$s', 'somtijds-social-icons' ),
			'dismiss'                         => __( 'Dismiss this notice', 'somtijds-social-icons' ),
			'notice_cannot_install_activate'  => __( 'There are one or more required or recommended plugins to install, update or activate.', 'somtijds-social-icons' ),
			'contact_admin'                   => __( 'Please contact the administrator of this site for help.', 'somtijds-social-icons' ),

			'nag_type'                        => '', // Determines admin notice type - can only be one of the typical WP notice classes, such as 'updated', 'update-nag', 'notice-warning', 'notice-info' or 'error'. Some of which may not work as expected in older WP versions.
		),
		*/
	);

	tgmpa( $plugins, $config );
}

if (!file_exists(WP_PLUGIN_DIR . '/kirki/kirki.php')) {
    return;
}

include_once( WP_PLUGIN_DIR . '/kirki/kirki.php' );

if ( class_exists( 'Kirki' ) ) {

	Kirki::add_config( 'ssi_customizer_options', array(
		'capability'  => 'edit_theme_options',
		'option_type' => 'option',
		'option_name' => 'ssi_customizer_options',
	) );

	Kirki::add_panel( 'ssi_customizer_panel', array(
		'priority'    => 90,
		'title'       => esc_attr__( 'Social Media Icons', 'ssi' ),
		'description' => esc_attr__( 'Add or modify social media icons options here' ),
	) );

	Kirki::add_section( 'ssi_customizer_url_section', array(
		'priority'    => 10,
		'title'       => esc_attr__( 'Profile Urls', 'ssi' ),
		'panel'       => 'ssi_customizer_panel',
		'description' => esc_attr__( 'Add the URLs to your profile pages here' ),
	) );

	Kirki::add_section( 'ssi_customizer_icons_section', array(
		'priority'    => 11,
		'title'       => esc_attr__( 'Icon Sets', 'ssi' ),
		'panel'       => 'ssi_customizer_panel',
		'description' => esc_attr__( 'Select which icons should be used' ),
	) );

	$channels = ssi_channels();

	foreach( $channels as $channel_label => $channel_name ) {
		
		Kirki::add_field( 'ssi_customizer_options', array(
			'type'        => 'url',
			'settings'    => 'ssi_' . strtolower( $channel_label ) . '_url',
			'label'       => esc_attr__( $channel_name . ' url', 'ssi' ),
			'description' => esc_attr__( 'Add the link to your ' . $channel_name . ' page here', 'ssi' ),
			'section'     => 'ssi_customizer_url_section',
		) );

	}

	Kirki::add_field( 'ssi_customizer_options', array(
		'type'        => 'select',
		'settings'    => 'ssi_icons',
		'label'       => esc_attr__( 'Select an option', 'ssi' ),
		'description' => esc_attr__( 'Select the icon set you want to use. Custom icons should be available in the assets/somtijds-social-icons/ folder in your (child) theme', 'ssi' ),
		'multiple'    => 0,
		'section'     => 'ssi_customizer_icons_section',
		'default'     => 'genericon',
		'choices' => array(
			'custom' => esc_attr__( 'From theme', 'ssi' ),
			'genericon' => esc_attr__( 'Genericons', 'ssi' ),
		),
	) );

}

function ssi_channels() {
	return array( 
		'codepen' => 'CodePen',
		'digg' => 'Digg',
		'dribbble' => 'Dribbble',
		'dropbox' => 'Dropbox',
		'ello' => 'ello',
		'facebook' => 'Facebook',
		'flickr' => 'Flickr',
		'foursquare' => 'Foursquare',
		'github' => 'GitHub',
		'googleplus' => 'Google +',
		'instagram' => 'Instagram',
		'linkedin' => 'LinkedIn',
		'mail' => 'E-Mail',
		'mailchimp' => 'MailChimp',
		'pinterest' => 'Pinterest',
		'pocket' => 'Pocket',
		'polldaddy' => 'Polldaddy',
		'reddit' => 'reddit',
		'skype' => 'Skype',
		'spotify' => 'Spotify',
		'stumbleupon' => 'StumbleUpon',
		'tumblr' => 'Tumblr',
		'twitch' => 'Twitch',
		'twitter' => 'Twitter',
		'vimeo' => 'Vimeo',
		'wordpress' => 'WordPress',
		'youtube' => 'YouTube',
	);
}

function ssi_get_icon( $icon_type = 'genericon', $channel_label, $profile_url ) {
	
	$channels = ssi_channels();

	if ( ! array_key_exists( $channel_label, $channels ) ) {
		return;
	}
	
	$icon = '<i class="' . $icon_type . ' ' . $icon_type . '-ssi-' . $channel_label . '"></i>';

	if ( $icon_type = 'custom' && file_exists( get_stylesheet_directory() . '/assets/somtijds-social-icons/' . $channel_label . '.png' ) ) {
		$icon = '<image src="' . get_stylesheet_directory_uri() . '/assets/somtijds-social-icons/' . $channel_label . '.png" alt="' . $channels[ $channel_label ] . ' logo" />';
	}
	return '<li style="list-style: none;"><a title="' . __( $channels[ $channel_label ], 'ssi' ) . '" href="' . $profile_url . '">' . $icon . '</a></li>';
}


function ssi_render_icons() {

	$settings = get_option( 'ssi_customizer_options' );

	$channels = ssi_channels();

	$icon_type = empty( $settings['ssi_icons'] ) ? 'genericon' : $settings['ssi_icons']; ?>

	<ul class="ssi_social_media_icons">
	<?php 
		foreach( $channels as $channel_label => $channel_name ) {
			
			$profile_url = isset($settings['ssi_' . $channel_label . '_url'])
                ? $settings['ssi_' . $channel_label . '_url']
                : '';
			if ( empty( $profile_url ) ) {
				continue;
			}
			echo ssi_get_icon( $icon_type, $channel_label, $profile_url );

		} 
	?>
	</ul><!-- end Somtijds Social Icons --> 
	<?php
}

add_action('ssi_render_icons','ssi_render_icons');

function ssi_maybe_enqueue_genericons_stylesheet() {

	if ( is_admin() ) {
		return;
	}

	$settings = get_option( 'ssi_customizer_options' );
	if ( empty( $settings['ssi_icons'] ) || $settings['ssi_icons'] === 'genericon' ) {
		wp_enqueue_style('ssi-genericons', plugins_url( 'somtijds-social-icons/assets/genericons/genericons/genericons.css' ), array(), (string) filemtime( plugin_dir_path( __FILE__ ) . 'assets/genericons/genericons/' ) );
	}
}

add_action( 'wp_enqueue_scripts','ssi_maybe_enqueue_genericons_stylesheet' );

/**
 * Add Social_Icons_Widget widget
 */
class Somtijds_Social_Icons_Widget extends WP_Widget {

    function __construct() {
        parent::__construct(
            'Social_Icons_Widget', // Base ID
            __( 'Social Media Icons', 'ssi' ), // Name
            array( 'description' => __( 'Social Media Icons Widget', 'ssi' ), ) // Args
        );
    }

    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args     Widget arguments.
     * @param array $instance Saved values from database.
     */
    public function widget( $args, $instance ) {
        echo $args['before_widget'];
        if ( ! empty( $instance['title'] ) ) {
            echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
        }
        ssi_render_icons();

        echo $args['after_widget'];
    }

    /**
     * Back-end widget form.
     *
     * @see WP_Widget::form()
     *
     * @param array $instance Previously saved values from database.
     */
    public function form( $instance ) {
        $title = isset($instance['title'])
            ? $instance['title']
            : '';
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
        </p>
        <?php
    }

    /**
     * Sanitize widget form values as they are saved.
     *
     * @see WP_Widget::update()
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     */
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

        return $instance;
    }

} // Class Publication_Widget

// Register Publication_Widget widget
function ssi_register_social_icons_widget() {
    register_widget( 'Somtijds_Social_Icons_Widget' );
}

add_action( 'widgets_init', 'ssi_register_social_icons_widget' );
